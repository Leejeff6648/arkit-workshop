# Lab4：陰影

### 4-1 點選 Max.scn 出現編輯畫面
![](assets/4-1.png)

### 4-2 加 Directional，設定座標、角度
![](assets/4-2.png)

### 4-3 至 Attributes inspector，Enable shadows 打勾，Mode 設定 Deffered，Color 透明度調 70%
![](assets/4-3.png)

### 4-4 加 plane，設定座標、角度
![](assets/4-4.png)

### 4-5 至 Material inspector，設定 Write To Color，只勾選 Alpha
![](assets/4-5.png)

### 4-6 跑實機看結果

### [Lab4 完成專案下載](https://gitlab.com/jackychenkiki/arkit-workshop/-/archive/Lab4/arkit-workshop-Lab4.zip)