# Lab5：搖桿控制

## _ARGameViewController_

### 5-1 touchesBegan 內容先註解掉（會被干擾）

### 5-2 新增類別屬性：controlViewDirection
```swift
var controlViewDirection = SIMD2<Float>(repeating: 0)
```

### 5-3 覆寫 touchesMoved 方法，計算搖桿的向量參數
```swift
override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
{
	guard touches.first?.view == controlView else{return }
	
	guard let point = touches.first?.location(in: controlView) else {return}
	
	let center = ( x: controlView.bounds.width / 2, y: controlView.bounds.height / 2 )
	
	let x = point.x - center.x
	let y = point.y - center.y
	
	let direction = SIMD2<Float>( Float(x), Float(y) )
	
	controlViewDirection = normalize(direction)
}
```

### 5-4 覆寫 touchesEnded 方法，將向量歸零
```swift
override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
{
	guard touches.first?.view == controlView else{return }
	
	controlViewDirection = SIMD2<Float>(repeating: 0)
}
```

### 5-5 實作 SCNSceneRendererDelegate，將向量參數傳遞給 max
```swift
func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval)
{
	max.walkInDirection(direction: maxDirection, time: time)
}
```

### 5-6 新增計算屬性 maxDirection - 將搖桿向量轉換為以相機為準的向量
```swift
var maxDirection: SIMD3<Float>
{
	var direction = SIMD3<Float>(controlViewDirection.x, 0, controlViewDirection.y)
	
	if let pointOfView = sceneView.pointOfView
	{
		let p0 = pointOfView.presentation.convertPosition(SCNVector3Zero, to: nil)
		let p1 = pointOfView.presentation.convertPosition(SCNVector3(direction), to: nil)
	
		direction = SIMD3<Float>(p1.x - p0.x, 0.0, p1.z - p0.z)
	
		if direction != SIMD3<Float>(repeating: 0.0)
		{
			direction = normalize(direction)
		}
	}

	return direction
}
```
------

## _Max_

### 5-7 新增類別屬性：previousTime
```swift
var previousTime = TimeInterval(0.0)
```

### 5-8 新增 walkInDirection，將向量作用在 Max 模型上
```swift
func walkInDirection(direction: SIMD3<Float>, time: TimeInterval)
{
	let deltaTime = Float( min( time - previousTime, 1/60) )
	let speed = deltaTime * 0.15
	previousTime = time
	
	if direction == SIMD3<Float>(repeating: 0.0)
	{
		isWalking = false
	}
	else
	{
		isWalking = true

		// 位移
		let pos = SIMD3<Float>(self.position)
		position = SCNVector3(pos + direction * speed)

		// 旋轉角度
		let angle = atan2f(direction.x, direction.z)
		let rotate = SCNAction.rotateTo(x: 0.0, y: CGFloat(angle), z: 0, duration: 0.1, usesShortestUnitArc: true)
		runAction(rotate)
	}
}
```


