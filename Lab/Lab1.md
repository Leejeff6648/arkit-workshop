# Lab1：AR 專案初始設定

### 1-1 下載並開啟 Starter project
#### [Starter-project](https://gitlab.com/jackychenkiki/arkit-workshop/-/archive/starter/arkit-workshop-starter.zip)

### 1-2 修改 bundle id，讓專案可以 build 實機
![](assets/1-2-1.png)
![](assets/1-2-2.png)

### 1-3 到 plist 設定 private-camera 授權請求
![](assets/1-3.png)

### 1-4 建立 scnassets 資料夾
![](assets/1-4-0.png)
![](assets/1-4-1.png)
![](assets/1-4-2.png)

### 1-5 除了 kuma.png，其他檔案拉進專案
![](assets/1-5.png)

### 1-6 dae 轉 scn 檔，會產生一個 copy 檔，重新命名為 max 並刪除 dae 檔
![](assets/1-6.jpg)

### 1-7 將 Shading 設定為 Physically Based，Diffuse 設定烘培圖檔
![](assets/1-7.png)

### 1-8 在 Assets 新增 AR Resources
![](assets/1-8.jpg)

### 1-9 新增 kuma 圖，並設定尺寸為 10 X 10 centermeters
![](assets/1-9.png)

### PS：3D 模型 和 kuma 圖都在 Resources 資料夾
