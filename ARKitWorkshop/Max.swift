//
//  Max.swift
//  ARKitWorkshop
//
//  Created by JackyChen on 2019/9/6.
//  Copyright © 2019 labo. All rights reserved.
//

import UIKit
import ARKit

class Max: SCNNode
{
    // MARK: - properties
    
    // 3-3 新增類別屬性：idleAnimation 和 walkAnimation
    var idleAnimation: SCNAnimationPlayer!
    
    var walkAnimation: SCNAnimationPlayer!
    
    // 5-7 新增類別屬性：previousTime
    var previousTime = TimeInterval(0.0)
    
    // 3-7 新增類別屬性：isWalking，並作 didSet 處理
    var isWalking = false
    {
        didSet
        {
            if oldValue != isWalking
            {
                isWalking ? walk() : idle()
            }
        }
    }
    
    // MARK: - init
    
    // 3-1 初始化設定
    init(scale: Float)
    {
        super.init()
        
        setUpModel(scale)
        loadAnimations()
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - private function

extension Max
{
    // 3-2 載入模型，並設定縮放比例
    private func setUpModel(_ scale: Float)
    {
        guard let scene = SCNScene(named: "art.scnassets/max.scn")else{fatalError("no Max scene") }
        
        guard let holderNote = scene.rootNode.childNode(withName: "Max_rootNode", recursively: true)else{fatalError("no Max_rootNode") }
        
        holderNote.scale = SCNVector3(SIMD3<Float>(repeating: scale))
        
        self.addChildNode(holderNote)
    }
    
    // 3-4 載入動作
    private func loadAnimations()
    {
        idleAnimation = loadAnimation(fromSceneNamed: "art.scnassets/max_idle.scn")
        walkAnimation = loadAnimation(fromSceneNamed: "art.scnassets/max_walk.scn")
        
        self.addAnimationPlayer(idleAnimation, forKey: "idle")
        self.addAnimationPlayer(walkAnimation, forKey: "walk")
        
        idleAnimation.play()
        walkAnimation.stop()
    }
    
    private func loadAnimation(fromSceneNamed sceneName: String) -> SCNAnimationPlayer
    {
        let scene = SCNScene( named: sceneName )!
        
        var animationPlayer: SCNAnimationPlayer! = nil
        
        scene.rootNode.enumerateChildNodes { (child, stop) in
            if !child.animationKeys.isEmpty
            {
                animationPlayer = child.animationPlayer(forKey: child.animationKeys[0])
                stop.pointee = false
            }
        }
        return animationPlayer
    }
    
    // 3-5 建立 walk 方法
    private func walk()
    {
        walkAnimation.play()
        idleAnimation.stop(withBlendOutDuration: 0.3)
    }
    
    // 3-6 建立 idle 方法
    private func idle()
    {
        idleAnimation.play()
        walkAnimation.stop(withBlendOutDuration: 0.3)
    }
}

// MARK: - public function

extension Max
{
    // 5-8 新增 walkInDirection，將向量作用在 Max 模型上
    func walkInDirection(direction: SIMD3<Float>, time: TimeInterval)
    {
        let deltaTime = Float( min( time - previousTime, 1/60) )
        let speed = deltaTime * 0.15
        previousTime = time
        
        if direction == SIMD3<Float>(repeating: 0.0)
        {
            isWalking = false
        }
        else
        {
            isWalking = true
            
            // 位移
            let pos = SIMD3<Float>(self.position)
            position = SCNVector3(pos + direction * speed)
            
            // 旋轉角度
            let angle = atan2f(direction.x, direction.z)
            let rotate = SCNAction.rotateTo(x: 0.0, y: CGFloat(angle), z: 0, duration: 0.1, usesShortestUnitArc: true)
            runAction(rotate)
        }
    }
}
